from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from django.utils import timezone


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {"todo_list": todo_list}
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        name = request.POST["name"]
        todo_list = TodoList.objects.create(
            name=name, created_on=timezone.now()
        )
        return redirect("todo_list_detail", id=todo_list.id)

    return render(request, "todos/todo_list_create.html")


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        name = request.POST["name"]
        todo_list.name = name
        todo_list.save()
        return redirect("todo_list_detail", id=todo_list.id)

    context = {"todo_list": todo_list}
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    context = {"todo_list": todo_list}
    return render(request, "todos/todo_list_delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        task = request.POST["task"]
        due_date = request.POST.get(
            "due_date",
        )
        due_date = None if due_date == "" else due_date
        is_completed = request.POST.get("is_completed", False) == "on"
        list_id = request.POST.get("list")

        todo_list = TodoList.objects.get(id=list_id)
        todo_item = TodoItem.objects.create(
            task=task,
            due_date=due_date,
            is_completed=is_completed,
            list=todo_list,
        )
        return redirect("todo_list_detail", id=todo_list.id)

    context = {"todo_lists": TodoList.objects.all()}
    return render(request, "todos/todo_item_create.html", context)
